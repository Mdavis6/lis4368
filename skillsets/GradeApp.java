import java.util.Scanner;

import javax.swing.plaf.metal.MetalToolTipUI;

public class GradeApp
{
    public static void main(String[] args) {

        Scanner sc = new Scanner (System.in);
        double grade = 0; //intialize sum of grades
        double gradeTotal = 0;  //grade total so far
        int count = 0;
        double average = 0;

        System.out.print("Please enter grades that range from 0 to 100. \nGrade average and total is rounded to 2 decimal places. \nNote: Program does *not* check for non-numerical characters.\n\n");

        while (grade != -1)
        {
            System.out.print("Enter grade or -1 to quit: ");
            grade = sc.nextDouble();

            if (grade == -1)
                break;

            while (grade < 0 || grade > 100) {
                // print invalid entry
                System.out.println("Invalid entry, not counted.");
                
                System.out.print("Enter grade or -1 to quit: ");
                grade = sc.nextDouble();
            }//end while

            gradeTotal = gradeTotal + grade;   
            count++;           
        }//end while

        //calculate average
        // print output format to 2 decimal
        // printf("Grade Count:\t %d\nGrade Total:\t %.2f", count, total)       
        average = gradeTotal / count;
        System.out.printf("\nGrade Count:\t %d\nGrade Total:\t %.2f\nAverage grade:\t%.2f\n", count, gradeTotal, average);
    }//end main
}//end class