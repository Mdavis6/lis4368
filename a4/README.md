# LIS4368 Advance Web Application Development

## Marie Davis

### Class Number Requirements:

1. Add JQuery validation and regular expressions
2. Update Customer.java and CustomerServlet.java
3. Compile Customer.java and CustomerServlet.java
4. Screenshots of failed and passed validations.
5. Skillsets

#### Screenshots

* Empty Form Requirements Screenshot
* Thanky you Form Screenshot
* Skillsets


#### A4 Screen and Links:
*Screenshot of Form Requirements*:

![No Data Validation](img/a4nodata.PNG "No Data Form")


*Screenshot of Passed Validation/Thank You page*:

![Passed Validation](img/a4validation.PNG "Thank you page")

*Skill Sets*

| Count Characters S10 |
| ---------- |
![](skillsets/CountCharacter.gif "Java Count Characters")

| FileWriteReadCountWords S11 | 
| ---------- | 
![](skillsets/FileWriteReadCountWords.gif "Java FileWriteReadCountWords") 

| Ascii S12 |
| ---------- |
![](skillsets/Ascii.gif "Java Ascii") 



*A4 Link*:
[A4 Local lis4368 Webapp](http://http://localhost:9999/lis4368/customerform.jsp?assign_num=a4 "A4 local lis4368 Webapp")
