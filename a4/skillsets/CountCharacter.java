import java.util.Scanner;
//Java program to count the letters, numbers, spaces of characters in a string
public class CountCharacter {
    
 public static void main(String[] args) {
        
    System.out.println("Program counts numbers and types of characters: that is, letters, spaces, numbers and other characters.");
    System.out.println("Hint: you may find the following methods helpful: isLetter(), isDigit(), isSpaceChar()");
    System.out.println("Additionally, you could add testing for upper and lower case letters");
    System.out.println("");
    System.out.println("");
         //scanner object
         Scanner scnr = new Scanner(System.in);

         System.out.print("Please enter your String: ");
         String input = scnr.nextLine();

		String test = "" + input;
		count(test);

	}

	public static void count(String data){
		char[] ch = data.toCharArray();
		int letter = 0;
		int space = 0;
		int num = 0;
		int other = 0;
		for(int i = 0; i < data.length(); i++){
			if(Character.isLetter(ch[i])){
				letter ++ ;
			}
			else if(Character.isDigit(ch[i])){
				num ++ ;
			}
			else if(Character.isSpaceChar(ch[i])){
				space ++ ;
			}
			else{
				other ++;
			}
		}
		System.out.println("Your String: " + data + "has the following number and types of characters.");
		System.out.println("letter(s): " + letter);
		System.out.println("space(s): " + space);
		System.out.println("number(s): " + num);
		System.out.println("other character(s): " + other);
			}
}