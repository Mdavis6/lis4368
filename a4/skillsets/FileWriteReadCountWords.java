import java.util.Scanner;
import java.io.*;


public class FileWriteReadCountWords{

    public static void main (final String args[]) throws Exception {
        // Scanner scnr = new Scanner(System.in);

        System.out.println("Progam captures user input, writes to and reads from same file, and counts number of words in file.");
        System.out.println("Hint: use hasNext() method to read number of words (tokens).");
        System.out.println();

        String myFile = "filecountwords.txt";

        try {
            //creat file object
            File file = new File(myFile);

            //create PrintWriter
            PrintWriter writer = new PrintWriter(file);

            //scanner object
            Scanner scnr = new Scanner(System.in);

            //string to store input
            String input = "";

            System.out.print("\nPlease enter text: ");
            input = scnr.nextLine();

            //write to file
            writer.write(input);

            System.out.println("Saved to file \"" + myFile + "\"");

            writer.close();
            
            Scanner read = new Scanner(new FileInputStream(file));
            
            //creat counter for num words
            int count = 0;

            while(read.hasNext()) {
                read.next();
                count++;
            }//end while loop

            System.out.println("Number of words: " + count);

        }//end TRY

        catch(IOException ex) {
            System.out.println("Error when writing to the file '" + myFile + "'");
            //ex.printStackTrace();
        }//end CATCH  
    } // end of MAIN
}

