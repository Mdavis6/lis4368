import java.util.*;
import java.io.*;

public class Ascii{

    public static void main (String args[]) {

        //declare and initialize variables
        int num =0;
        boolean isValidNum = false;
        Scanner scnr = new Scanner(System.in);

        //char to ascii
        System.out.println("\nPrinting characters A-Z as ASCII values:");

        //loop printing ascii values
        for (char character = 'A'; character <= 'Z'; character++) {
            System.out.printf("Character %c has acii value %d\n", character, ((int)character));
        }

        //ascii to char
        System.out.println("\nPrinting ACSII values 48-112 as characters:");

        //loop printing values
        for(num = 48; num <= 122; num++) {
            System.out.printf("ASCII value %d has character value %c\n", num, ((char)num));
        }

        //allow user input
        System.out.println("\nAllowing user ASCII value input:");

        while(isValidNum == false) {
            System.out.print("Please enter ASCII value (32-127): ");

            

            try {
            num = scnr.nextInt();
            isValidNum = true;

            //if not a valid number
            if (num < 32 || num > 127) {
                System.out.println("ASCII value must be >= 32 and <= 127.\n");
                isValidNum = false;
            }

            else if (isValidNum == true) {
                System.out.println();

                //display result
                System.out.printf("ASCII value %d has character value %c\n\n", num, ((char)num));
            }

        }

        //if not a number
        catch(InputMismatchException ex) {
            System.out.println("Invalid integer -- ASCII value must be a number.\n");
            isValidNum = false;
            scnr.next();
        }//end CATCH  
    

        }//end while loop

    }//end MAIN

}//end ASCII