> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advance Web Application Development

## Marie Davis

### Class Number Requirements:

1. Add JQuery validation and regular expressions
2. Use regexp to only certain characters for each field. 
3. Research validation codes.
4. Screenshots of failed and passed validations.
5. Update carousel images.

#### Screenshots

* Failed Validation Screenshot
* Pass Validation Screenshot
* Carousel images

>
>

#### Project 1 Screen and Links:
*Screenshot of Failed Validation*:

![Failed Validation](img/p1fail.PNG "Failed Validation Form")


*Screenshot of Passed Validation*:

![Passed Validation](img/p1indexpage.PNG "Passed Validation Form")

*Carousel Images*

| Carousel 1 | 
| ---------- | 
![](img/c1.png "Carousel 1") 

| Carousel 2 |
| ---------- |
![](img/c2.png "Carousel 2") 

| Carousel 3 |
| ---------- |
![](img/c3.png "Carousel 3")

*Project 1 Link*:
[P1 Local lis4368 Webapp](http://localhost:9999/lis4368/p1/index.jsp "P1 local lis4368 Webapp")