# LIS4368 Advance Web Application Development

## Marie Davis

### Class Number Requirements:

1. Write and Compile DBUtil.java, ConnectionPool.java, and CustomerDB.java
2. Update CustomerServlet.java and context.xml
3. Add new customer to database
4. Check database for new entry
5. Skillsets (Java Apps Below)

#### Screenshots

* Valid User Form Screenshot
* Thank you Form Screenshot
* Database Entry Screenshot
* Skillsets


#### A5 Screen and Links:
*Screenshot of Valid User Form*:

![No Data Validation](img/validationform.PNG "No Data Form")

*Screenshot of Passed Validation/Thank You page*:

![Passed Validation](img/thankyoupage.PNG "Thank you page")

*Screenshot of Associated Database Entry*:

![Passed Validation](img/customerbefore.PNG "Customer Before")
![Passed Validation](img/customerafter.PNG "Customer After")

*Skill Sets*

| NumberSwap S13 |
| ---------- |
![](img/NumberSwap.PNG "Number Swap App")

| Largest of Three Numbers S14 | 
| ---------- | 
![](img/LargestofThree.PNG "Largest of Three Numbers") 

| Simple Calculator Using Methods S15 |
| ---------- |
![](img/SimpleCalculator.PNG "Simple Calculator") 



*A5 Link*:
[A5 Local lis4368 Webapp](http://http://localhost:9999/lis4368/customerform.jsp?assign_num=a5 "A5 local lis4368 Webapp")

