//Marie Davis SS13 LIS4368

import java.util.*;
import java.io.*;

public class NumberSwap{

    public static void main (String args[]) {

        //declare variables
        int num1 = 0, num2 = 0, temp;

        Scanner scnr = new Scanner(System.in);

        System.out.println("Program swaps two integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");

        //enter num1
        System.out.print("\nPlease enter first number: ");

        while (!scnr.hasNextInt()) {        
            scnr.next(); // Read and discard offending non-int input
            System.out.println("Not valid integer!");
            System.out.print("\nPlease try again. Enter first number: ");
        }

        // At this point in the code, the user has entered an integer
        num1 = scnr.nextInt(); // Get the integer


        //enter num2
        System.out.print("\nPlease enter second number: ");

        while (!scnr.hasNextInt()) {        
            scnr.next(); // Read and discard offending non-int input
            System.out.println("Not valid integer!");
            System.out.print("\nPlease try again. Enter second number: ");
        }

        // At this point in the code, the user has entered an integer
        num2 = scnr.nextInt(); // Get the integer


        //print report
        System.out.println("\nBefore Swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);

        temp = num1;
        num1 = num2;
        num2 = temp;

        System.out.println("\nAfter Swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);
        System.out.println();


    }
}