//Marie Davis SS14

import java.util.*;
import java.io.*;

public class LargestThreeNumbers{

    public static void main (String args[]) {

        //declare variables
        int num1 = 0, num2 = 0, num3 = 0;

        Scanner scnr = new Scanner(System.in);

        System.out.println("Program swaps two integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");

        //enter num1
        System.out.print("\nPlease enter first number: ");

        while (!scnr.hasNextInt()) {        
            scnr.next(); // Read and discard offending non-int input
            System.out.println("Not valid integer!");
            System.out.print("\nPlease try again. Enter first number: ");
        }

        num1 = scnr.nextInt(); // Get the integer


        //enter num2
        System.out.print("\nPlease enter second number: ");

        while (!scnr.hasNextInt()) {        
            scnr.next(); // Read and discard offending non-int input
            System.out.println("Not valid integer!");
            System.out.print("\nPlease try again. Enter second number: ");
        }

        num2 = scnr.nextInt(); // Get the integer

        //enter num3
         System.out.print("\nPlease enter third number: ");

         while (!scnr.hasNextInt()) {        
             scnr.next(); // Read and discard offending non-int input
             System.out.println("Not valid integer!");
             System.out.print("\nPlease try again. Enter third number: ");
         }

         num3 = scnr.nextInt(); // Get the integer


        //find which number is the largest
        if (num1 > num2 && num1 > num3) {
            System.out.println("\nFirst number is largest.");
        }
        else if (num2 > num1 && num2 > num3) {
            System.out.println("\nSecond number is largest.");
        }
        else if (num3 > num1 && num3 > num2) {
            System.out.println("\nThird number is largest.");
        }
        else {
            System.out.println("\nIntegers are equal.");
        }

        System.out.println();
      

    }
}