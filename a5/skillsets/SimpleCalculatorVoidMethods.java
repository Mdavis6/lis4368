//Marie Davis LIS4368

    import java.io.*;
    import java.util.*;
    
    public class SimpleCalculatorVoidMethods {       
        public static void main(String[] args) throws Exception {
            //Scanner Creation
            Scanner scnr = new Scanner(System.in);
            //Variable Declaration
            String operation = "";
            double num1 = 0.0, num2 = 0.0;

            //Introduction
            System.out.println("Program uses nonvalue-returning methods to add, subtract, multiply, divide and power floating point numbers, rounded to two decimal places.");
            System.out.println("Note: Program checks for integers and non-numeric values and division by zero\n");

            System.out.print("Enter mathematical operation (a=add, s=subtract, m=multiply, d=divide, p=power): ");
            operation = scnr.next().toLowerCase();

            while(!operation.equals("a") && !operation.equals("s") && !operation.equals("m") && !operation.equals("d") && !operation.equals("p")) {
                System.out.print("\nIncorrect operation. Please enter correct operation: ");
                operation = scnr.next();
            }

            //first number
            System.out.print("\nPlease enter first number: ");

            while (!scnr.hasNextDouble()) {        
                System.out.println("Not valid number!\n");
                scnr.next(); 
                System.out.print("\nPlease try again. Enter first number: ");
            }

            num2 = scnr.nextDouble(); // Get the integer

            //second number
            System.out.print("\nPlease enter second number: ");

            while (!scnr.hasNextDouble()) {        
                System.out.println("Not valid number!\n");
                scnr.next(); 
                System.out.print("\nPlease try again. Enter second number: ");
            }

            num2 = scnr.nextDouble(); // Get number
    
            //determine operation and call fuctions
            if (operation.equals("a")) {
                Add(num1, num2);
            }
            else if (operation.equals("s")){ 
                Subtract(num1, num2);
            }
            else if (operation.equals("m")){ 
                Multiply(num1, num2);
            }
            else if (operation.equals("d")){ 
                Divide(num1, num2);
            }  
            else if (operation.equals("p")){
                Power(num1, num2);
            }
        }//end main

        //define functions for menu choices

        public static void Add (double num1, double num2) {
            double sum = 0.00;
    
            sum = num1 + num2;
    
            System.out.println();
            System.out.println("The sum of " + num1 + " + " + num2 + " = " + String.format("%.2f", sum));
            System.out.println();
        }//end add
    
        public static void Subtract (double num1, double num2) {
            double dif = 0.00;
    
            dif = num1 - num2;
    
            System.out.println();
            System.out.println("The difference of " + num1 + " - " + num2 + " = " + String.format("%.2f", dif));
            System.out.println();
        }//end subtract
    
        public static void Multiply (double num1, double num2) {
            double product = 0.00;
    
            product = num1 * num2;
    
            System.out.println();
            System.out.println("The product of " + num1 + " x " + num2 + " = " + String.format("%.2f", product));
            System.out.println();
        }//end multiply
    
        public static void Divide (double num1, double num2) {
            double quotient = 0.00;
    
            quotient = num1 / num2;
    
            if (num2 == 0.00) {
                System.out.println();
                System.out.println("Cannot divide by zero!\n");
                System.exit(0);
            }
            else {
                System.out.println();
                System.out.println("The quotient of " + num1 + " / " + num2 + " = " + String.format("%.2f", quotient));
                System.out.println();
            }
        }//end divide
    
        public static void Power (double num1, double num2) {
            double power = Math.pow(num1, num2);

            System.out.println();
            System.out.println(num1 + " to the power of " + num2 + " = " + String.format("%.2f", power));
            System.out.println();
        }//end power
    }//end class
