> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advance Web Application Development

## Marie Davis

### Class Number Requirements:

*Assignment Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")    
    - Install Git
    - Create BitBucket Repo
    - Install AMMPS
    - Install JDK
    - Install Tomcat
    - Provide screenShots of installations
    - Complete Bitbucket tutorials
    - Provide Git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Local MySQL
    - Install MySQL JDBC Driver
    - Create ebookshop Database
    - Create and Compile HelloServlet and QueryServlet
    - Connect/Show Database Servlets
    - Add background to Index File
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Entity Relationship Diagram (ERD)
    - Include Data in Each Table
    - Forward Engineer MySql 

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Add JQuery validation and regular expressions
    - Update Customer.java and CustomerServlet.java
    - Compile Customer.java and CustomerServlet.java
    - Screenshots of failed and passed validations.
    - Skillsets
	
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Write and Compile DBUtil.java, ConnectionPool.java, and CustomerDB.java
    - Update CustomerServlet.java and context.xml
    - Add new customer to database
    - Check MySQL for database entry
    - Skillsets

*Project Work Links:*

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Modify index.jsp
    - Add jQuery validation and regular expressions
    - Update regexp for appropriate characters
    - Research validation
    - Add Screen Shots of Failed and Passed Validations
    - Add Screen Shots of Carousel

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modify and Compile CustomerServlet.java
    - Modify and Compile CustomerDB.java
    - Update modify.jsp and customer.jsp
    - Update, Delete, Add Customers to Database


*Additional Work Links:*
 [Skillsets README.md](skillsets/README.md "My Skillsets README.md file")
