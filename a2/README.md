# LIS4368 Advance Web Application Development

## Marie Davis
### Assignment 2 Requirements:

*Items to Complete*

1. MySql Login/Installation
2. Create ebookshop database
3. Create and compile HelloServlet and QuerybookServlet
4. Compile Database using Servlets
5. Add background to index.jsp

#### README.md file should include the following items:

* localhost:9999/hello Screenshot and link
* localhost:9999/hello/sayhello Screenshot and link
* localhost:9999/hello/querybook.html Screenshot and link
* One Screenshot of the query results 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

> #### localhost:9999 links:

* (http://localhost:9999/hello)
* (http://localhost:9999/hello/HelloHome.html)
* (http://localhost:9999/hello/index.html)
* (http://localhost:9999/hello/sayhello)
* (http://localhost:9999/hello/querybook.html

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:
*Directory listed because of HelloHome.html*

![/hello screenshot](img/hello.png)

*Screenshot of http://localhost:9999/hello/index.html*:

![/hello/Index.html screenshot](img/Index.PNG)

*Screenshot of http://localhost:9999/hello/sayhello*:

![/hello/sayhello screenshot](img/sayhello.PNG)

*Screenshot of http://localhost:9999/hello/querybook.html*:

![/hello/querybook.html screenshot](img/querybook.PNG)

*Screenshot of Querybook Result*:

![querybook result Screenshot](img/querybookresults.PNG)

*Screenshot of A2 Index*
![A2 Index] (img/a2index.PNG)



#### Repo Links:

*Link to lis4368 BitBucket Repo*
[lis4368 BitBucket Repo](https://bitbucket.org/Mdavis6/lis4368/src/master/ "lis4368 Bitbucket repo")

*Link to Local lis4368 Web App:*
[A2 Local lis4368 web app](http://localhost:9999/lis4368/a2/index.jsp "Local lis4368 web app")
