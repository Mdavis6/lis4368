<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 19:38:14 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Marie Davis">
	<link rel="icon" href="img/favicon.ico">

	<title>LIS4368 - Assignment 2</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body background="img/bga2.jpg">



<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->
<%@ include file="/global/nav.jsp" %>

<div class="container">
	<div class="starter-template">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="page-header">
					<%@ include file="global/header.jsp" %>
				</div>
				<b>Servlet Compilation and Usuage:</b><br />
				<img src="img\sayhello.PNG" class="img-responsive" alt="sayhello" />
				<br /> <br />
				<b>localhost:9999/hello/querybook:</b><br />
				<img src="img\querybook.PNG" class="img-responsive" alt="Querybook" />
				<br /> <br />
				<b>Querybook Result</b><br /><br />
				<img src="img\querybookresults.PNG" class="img-responsive" alt="Querybook Result" />
				<b>A2 index</b><br /><br />
				<img src="img\a2index.PNG" class="img-responsive" alt="A2 Index Screenshot" />
				
				</div>
		</div>
<%@ include file="/global/footer.jsp" %>
```</div> <!-- end starter-template -->
</div> <!-- end container -->

<%@ include file="/js/include_js.jsp" %>
</body>
</html>

