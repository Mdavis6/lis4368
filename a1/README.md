> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advance Web Application Development

## Marie Davis
### Assignment 1 Requirements:

*Items to Complete*

1. Git and Bitbucket setup
2. Installations and ssh setup
3. Bitbucket Links to Repo

#### README.md file should include the following items:

* Bullet-list items
* Installations of Git, Bitbucket, AMPPS Install, Java, and Tomcat
* Screenshots of AMPPS Install, Java Hello, Tomcat (localhost:9999)
* Learn and work with Git commands 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: Command initializes Git in the folder where we are calling the command
2. git status: Status of the changes between the local directory and the repository
3. git add: Tells Git that we are including changes in the next commit 
4. git commit: Records changes that we've made in our working directory 
5. git push: Pushes a copy of all local repository content to the remote repository
6. git pull: It will copy the contents from teh remote repository branch and merge all the changes into the local branch
7. One additional git : git fetch: Copies the contents from the remote repository to your local repository
#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdkinstall.PNG)

*Screenshot of running https://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of index.jsp running*:

![Index.jsp Running](img/index.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mdavis6/bitbucketstationlocations/ "Bitbucket Station Locations")

*Link to Local lis4368 web app*
[A1 Local lis4368 web app](http://localhost:9999/lis4368/a1/index.jsp "Local lis4368 web app")