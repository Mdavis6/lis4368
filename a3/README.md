# LIS4368 Advance Web Application Development

## Marie Davis
### Assignment 3 Requirements:

*Items to Complete*

1. Entity Relationship Diagram (ERD)
2. Include Data in Each Table (10 entries per table)
3. Include A3 index image

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of A3 index
* Links to mwb and sql file

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of ERD*:

![A3 ERD Image](img/petstoreERD.PNG)


*Screenshot of A3 Index*
![A3 Index] (img/a3index.PNG)

*A3 docs: a3.mwb file* 
[A3 mwb](docs/a3.mwb "A3 ERD in .mwb format")


*A3.sql file*
[A3 sql](docs/a3.sql "A3 SQL Script")


#### Repo Links:

*Link to lis4368 BitBucket Repo*
[lis4368 BitBucket Repo](https://bitbucket.org/Mdavis6/lis4368/src/master/ "lis4368 Bitbucket repo")

*Link to Local lis4368 Web App:*
[A3 Local lis4368 web app](http://localhost:9999/lis4368/a3/index.jsp "Local lis4368 web app")
