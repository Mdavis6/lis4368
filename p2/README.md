 **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marie Davis	

### Project #2 Requirements:

*Deliverables:*

1. Modify and Compile CustomerServlet.java
2. Modify and Compile CustomerDB.java
3. Use regexp to only allow appropriate characters for each control
4. Link Database
5. Provide Screenshots demonstrating inserting, updating, and deleting abilities


#### README.md file should include the following items:

* Screenshot of Populated Form
* Screenshot of Passed Server-side validation
* Screenshot of Update and Delete Buttons Working
* Screenshot of Updated Database

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*All functions*:
![sample gif] (img/P2sample.gif)

*Valid User Entry Form (add customer)*:          |  *Passed validation*:
:-------------------------:|:-------------------------:
![Valid User Entry Form] (img/addcustomer.PNG) | ![Passed Validation (thanks.jsp)](img/typageadd.PNG)

*Delete Warning*:
![Delete Warning](img/deletecustomer.PNG)

*Display Data*:
![Display Data](img/displaydata.PNG)


*Modified Data*:
![Modified Data](img/finaldb.PNG)


*Associated Database Changes (Select, Insert, Update, Delete)*:
![Associated Database Changes (Select, Insert, Update, Delete)](img/changes.PNG)


#### Links:

*Bitbucket Repo:*
[lis4368 Bitbucket Repo](https://bitbucket.org/mdavis6/lis4368/ "lis4368 Bitbucket Repo")

*LIS 4368 Local Link:*
[localhost:9999/lis4368](http://localhost:9999/lis4368/ "localhost:9999/lis4368")